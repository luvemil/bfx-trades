var rp = require('request-promise');
var _ = require('underscore');
var moment = require('moment');

var Module = (function() {
  var url = "https://api.bitfinex.com/v2";

  var getTrades = function(start_date) {
    return rp(`${url}/trades/tBTCUSD/hist?start=${moment(start_date).unix() * 1000}&sort=1`)
  }

  return {
    apiUrl: url,
    getTrades: getTrades,
  }

})()

module.exports = Module;
